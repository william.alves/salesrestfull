API RESTful
===========

Un microservicio que exponga API RESTful utilizando Python/Flask

*Llamadas*

|URL|METHOD|DESCRIPTION|
|---|------|-----|
|/user/      |POST|Crear usuarios, indicando email, nombre, apellido y dirección. *Parametros*: email, nombre, apellido, direccion|
|/user/email |PUT|Modificar un usuario indicando su email y los campos a modificar. *Parametros*: nombre, apellido, direccion|
|/user/email |DELETE|Deshabilitar un usuario indicando su email|
|/user/email |PATCH|Reactivar un usuario indicando su email|
|/sale       |POST|Crear una venta asociada a un usuario. *Parametros*: user_email, amount|
|/sale/id    |GET|Obtener los detalles de una venta indicando su id|
|/sales/email|GET|Obtener el detalles de todas las ventas de un usuario indicando su email|
|/sale/id    |DELETE|Eliminar una venta indicando su identificador único|
|/user       |GET|Listar todos los usuarios, indicando su email, nombre, apellido, dirección, estado (si fue aprobado y/o deshabilitado), la cantidad total de ventas asociadas a dicho usuario y el importe total operado (suma de los importes de todas las ventas no anuladas).|
|/sales      |GET|Obtener un listado de todas las ventas y su detalle|
