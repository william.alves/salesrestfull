import unittest
import requests
import json
from flask import Flask
from flask_pymongo import PyMongo
from libs.users import User

app = Flask(__name__)

app.config['MONGO_DBNAME'] = 'sales'
#app.config['MONGO_URI'] = 'mongodb://localhost:27017/sales'
app.config['MONGO_URI'] = 'mongodb://datastore:27017/sales'

mongo = PyMongo(app)


class RestfulTestCase(unittest.TestCase):

        def setUp(self):
            self.url = 'http://127.0.0.1:5000'
            with app.app_context():
                mongo.db.users.delete_many({})
                mongo.db.sales.delete_many({})

        def tearDown(self):
            with app.app_context():
                mongo.db.users.delete_many({})
                mongo.db.sales.delete_many({})
            pass

        """User list, response code 200"""
        def test_user_200(self):
            response = requests.get( self.url +  '/user')
            self.assertEqual(response.status_code, 200)

        def test_add_email_empty(self):
            payload = {'email': '', 'nombre':'Pedro', 'apellido': 'Benitez', 'direccion':'dragones 7442'}
            r = requests.post(self.url + '/user', json=payload)
            self.assertEqual(r.status_code, 400)

        def test_add_email_empty_message(self):
            payload = {'email': '', 'nombre': 'Pedro', 'apellido': 'Benitez', 'direccion': 'dragones 7442'}
            r = requests.post(self.url + '/user', json=payload)
            rjson = json.loads(r.text)
            self.assertIn("Email is empty", rjson['result']['error_message'])

        def test_add_invalid_email(self):
            payload = {'email': 'gerardobenitez', 'nombre':'Gerardo', 'apellido': 'Benitez', 'direccion':'dragones 7442'}
            r = requests.post(self.url + '/user', json=payload)
            self.assertEqual(r.status_code, 400)

        def test_add_invalid_email_message(self):
            payload = {'email': 'gerardobenitez', 'nombre': 'Gerardo', 'apellido': 'Benitez', 'direccion': 'dragones 7442'}
            r = requests.post(self.url + '/user', json=payload)
            rjson = json.loads(r.text)
            self.assertIn('Email is not valid', rjson['result']['error_message'])

        def test_add_user_200(self):
            payload = {'email': 'gerardobenitez@gmail.com', 'nombre': 'Gerardo', 'apellido': 'Benitez', 'direccion': 'dragones 7442'}
            r = requests.post(self.url + '/user', json=payload)
            self.assertEqual(r.status_code, 201)

        def test_200_get_valid_user(self):
            payload = {'email': 'gerardobenitez@gmail.com', 'nombre': 'Gerardo', 'apellido': 'Benitez',
                       'direccion': 'dragones 7442'}
            r = requests.post(self.url + '/user', json=payload)
            r = requests.get(self.url + "/user/gerardobenitez@gmail.com")
            self.assertEqual(r.status_code, 200)

        def test_get_valid_user(self):
            payload = {'email': 'gerardobenitez@gmail.com', 'nombre': 'Gerardo', 'apellido': 'Benitez',
                       'direccion': 'dragones 7442'}
            requests.post(self.url + '/user', json=payload)
            r = requests.get(self.url + "/user/gerardobenitez@gmail.com")
            rjson = r.json()
            self.assertEqual(rjson['result']['email'], 'gerardobenitez@gmail.com')

        def test_404_get_invalid_user(self):
            r = requests.get(self.url + "/user/pedrobenitez@gmail.com")
            self.assertEqual(r.status_code, 404)

        def test_update_invalid_user(self):
            payload = { 'nombre': 'Juan Gabriel', 'apellido': 'Benitez',
                       'direccion': 'dragones 7300'}
            r = requests.put(self.url + '/user/pepepedro@gmail.com', json=payload)
            self.assertEqual(r.status_code, 400)

        def test_update_valid_user(self):
            # insert user
            payload = {'email': 'gerardobenitez@gmail.com', 'nombre': 'Gerardo', 'apellido': 'Benitez',
                       'direccion': 'dragones 7442'}
            r = requests.post(self.url + '/user', json=payload)

            # update
            payload = { 'nombre': 'Gerardo Abel', 'apellido': 'Benitez Davalos',
                       'direccion': 'dragones 7442'}
            r = requests.put(self.url + '/user/gerardobenitez@gmail.com', json=payload)
            self.assertEqual(r.status_code, 200)

        def test_add_valid_sale(self):
            # insert user
            payload = {'email': 'gerardobenitez@gmail.com', 'nombre': 'Gerardo', 'apellido': 'Benitez',
                       'direccion': 'dragones 7442'}
            r = requests.post(self.url + '/user', json=payload)

            # insert sale
            payload = {'user_email': 'gerardobenitez@gmail.com', 'amount': '600'}
            r = requests.post(self.url + '/sale', json=payload)

            self.assertEqual(r.status_code, 201)

        def test_get_valid_sale(self):
            # insert user
            payload = {'email': 'gerardobenitez@gmail.com', 'nombre': 'Gerardo', 'apellido': 'Benitez',
                       'direccion': 'dragones 7442'}
            r = requests.post(self.url + '/user', json=payload)

            # insert sale
            payload = {'user_email': 'gerardobenitez@gmail.com', 'amount': '600'}
            r = requests.post(self.url + '/sale', json=payload)
            rjson = r.json()
            id = rjson['result']['uuid']

            # get sale
            r = requests.get(self.url + '/sale/' + id)
            self.assertEqual(r.status_code, 200)

        def test_get_invalid_sale(self):
            # get sale
            r = requests.get(self.url + '/sale/2')
            self.assertEqual(r.status_code, 404)








if __name__ == '__main__':
    unittest.main()
